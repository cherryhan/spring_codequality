 set -e
  echo "***code quality gate $(ls -l)"
  echo ${CQRPT_URL}
  echo ${GITLAB_USER_LOGIN}
  echo ${GITLAB_USER_NAME}
  echo ${GITLAB_USER_ID}

# raw json report
sed -e 's/","/",\n\t"/g' -e 's/{"key"/\n\t{"key"/g' -e 's/":{"/":\n\t\t{"/g' -e 's/,"/,\n\t\t  "/g' sonar.json > report.json

# prepare for html report
sed -e 's/{"key/\n<hr><p>key/g' -e 's/","/\n<p>/g' -e 's/":"/: /g' -e 's/"total"/\n<br><p>total/g' -e 's/"//g' sonar.json > cqrpt.json
cp header.html report.html
echo ${CQ_URL} >> report.html
grep -E 'total|type:|<hr>|rule:|severity:|component:|project:|line:|textRange:|message: |effort:' cqrpt.json >> report.html

